Документация: [docs.md](./docs.md)

##Порядок встановлення

Переходимо в папку з доменами ( cd domains )

Клонуємо репозиторій, команду беремо з бітбакету:
![install](http://i.imgur.com/fa0prr0.png)

Команда повинна бути такою (замість {yourLogin} - ваш логін, yii2-miraclecms.loc - папка куди буде встановлений проект)

- `git clone https://{yourLogin}@bitbucket.org/valik619/yii2-miraclecms.git yii2-miraclecms.loc`

Переходим в папку з проектом (yii2-miraclecms.loc - замініть на вашу папку)
    
- `cd yii2-miraclecms.loc`

Підтягуємо бібліотеки за допомогою composer

- `composer install`
- `composer update`


Встановлюємо yii2. Вибираємо 0 - Development версію.

- `php init`


Дані від бд /common/config/main-local.php

####Виконуємо міграції:
    
- `php yii migrate`

Якщо у вас Windows та Apache то встановлення завершене. 

Логін/Пароль від адмінки: `admin`/`admin`
Адмінка по адресу: `/backend`

Для Linux-розробників:

Выставляем права доступа на RBAC

- `chmod 666 modules/rbac/data/assignments.php`
- `chmod 666 modules/rbac/data/items.php`
- `chmod 666 modules/rbac/data/rules.php`


Якщо у вас Nginx

    server {
        root /home/{username}/domains/{parent_domain}/frontend/web;

        if ($host ~* ^www\.(.*)$) {
          rewrite / http://$server_name$request_uri;
        }

        location ^~ /backend {
          rewrite ^/backend(.*)$ /backend/web$1 last;
        }

        location ^~ /backend/web {
          root /home/{username}/domains/{parent_domain};

          location ~ /\.(ht|svn|git) {
            deny all;
          }

          location ~ \.php$ {
            try_files $uri =404;
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $request_filename;
            fastcgi_intercept_errors on;
            fastcgi_pass unix:/var/run/php5-{domain}.sock;
          }
        }

        location / {
          try_files $uri $uri/ /index.php?$args;
        }

        location ~ ^/(protected|framework|themes/\w+/views) {
          deny  all;
        }

        location ~ /\. {
          deny all;
        }

        location ~ \.(js|css|png|jpg|gif|swf|ico|pdf|mov|fla|zip|rar)$ {
          try_files $uri =404;
        }

        location ~ \.php$ {
          fastcgi_split_path_info ^(.+\.php)(/.+)$;
          include fastcgi_params;
          fastcgi_param SCRIPT_FILENAME $request_filename;
          fastcgi_intercept_errors on;
          fastcgi_pass unix:/var/run/php5-{domain}.sock;
        }

    }

Створюємо сімлінк з frontend/web/files на common/files

###Увага!
Не забудьте замінити папку з сайтом "/path/to/example.loc" і домен "example.loc"