<?php

namespace console\controllers;

use DiDom\Query;
use modules\actors\models\frontend\Actors;
use modules\films\models\frontend\Films;
use modules\films\models\backend\Genres;
use modules\rating\models\frontend\Rating;
use modules\users\models\frontend\Users;
use yii\base\ErrorException;
use yii\console\Controller;
use yii\helpers\ArrayHelper as AH;
use yii\helpers\Console;
use DiDom\Document;
use yii\helpers\Url;

//chcp 65001

class RatingController extends Controller
{
    public function actionIndex()
    {
        $films = Films::find()->all();
        $actors = Actors::find()->all();
        $users = Users::find()->all();

        $totalFilms = count($films);
        $totalActors = count($actors);
        $totalUsers = count($users);

        foreach ($users as $user) {
            /**
             * @var $user Users
             * @var $film Films
             * @var $actor Actors
             */
            echo 'Current user: ' . $user->id .'/'.$totalUsers."\n";

            foreach ($films as $film) {
                echo 'Current film: ' . $film->id .'/'.$totalFilms."\n";

                $rating = $this->getMyRate('films', $actor->id, $user->id);
                if ($rating < 1) {
                    $vote = new Rating();
                    $vote->model = $film->id;
                    $vote->user_id = $user->id;
                    $vote->module = 'films';
                    $vote->rate = mt_rand(5,10);
                    $vote->save(0);
                }
            }
            foreach ($actors as $actor) {
                echo 'Current actor: ' . $actor->id .'/'.$totalActors."\n";

                $rating = $this->getMyRate('actors', $actor->id, $user->id);
                if ($rating < 1) {
                    $vote = new Rating();
                    $vote->model = $actor->id;
                    $vote->user_id = $user->id;
                    $vote->module = 'actors';
                    $vote->rate = mt_rand(5,10);
                    $vote->save(0);
                }
            }
        }
    }

    public function getMyRate($module, $model, $user_id)
    {
        if(null !== $rate = Rating::find()->where(compact('module', 'model', 'user_id'))->one()) {
            return abs(intval($rate->rate));
        }

        return 0;
    }
}