<?php

namespace console\controllers;

use DiDom\Query;
use modules\actors\models\frontend\Actors;
use modules\films\models\frontend\Films;
use modules\films\models\backend\Genres;
use modules\films\models\frontend\Links;
use modules\rating\models\frontend\Rating;
use modules\users\models\frontend\Users;
use yii\base\ErrorException;
use yii\console\Controller;
use yii\helpers\ArrayHelper as AH;
use yii\helpers\Console;
use DiDom\Document;
use yii\helpers\Url;

//chcp 65001

class LinksController extends Controller
{
    static $handlers = [
        Links::TYPE_WATCH => [
            //''
        ],
        Links::TYPE_DOWNLOAD => [
            'exua',
            'toloka',
        ],
    ];

    public function actionShort()
    {
        $films = Films::find()->offset(5)->all();
        $totalFilms = count($films);

        /**
         * @var $film Films
         * @var $link Links
         */
        foreach ($films as $film) {
            echo "{$film->id}/{$totalFilms}\n";
            $i=0;
            foreach ($film->getLinks()->all() as $link) {
                if($link->setShortUrl()->save(0)) {
                    $i++;
                    echo "Link #{$i}: {$link->original_url} - {$link->short_url}\n";
                } else {
                    echo 'xz 4o takoe';
                }
            }
        }
    }

    public function actionIndex()
    {
        $films = Films::find()
            ->offset(50)
            ->all();
        $totalFilms = count($films);

        /**
         * @var $film Films
         */
        foreach ($films as $film) {
            echo $film->id.'/'.$totalFilms;
            echo "\n";
            $this->getLinks($film);
        }
    }

    public function getLinks(Films $film)
    {
        foreach (self::$handlers as $handler_type => $handlers) {
            foreach ($handlers as $handler) {
                if (method_exists($this, $handler)) {
                    if (false !== $link = call_user_func([&$this, $handler], $film)){
                        $this->setLink($link, $handler_type, $film->id);
                    } else {
                        echo "{$film->original_name} not found in {$handler}\n";
                    }
                } else {
                    echo "Method {$handler} not found!\n";
                }
            }
        }
    }

    public function setLink($url, $type, $film_id)
    {
        $link = new Links();
        $link->film_id = $film_id;
        $link->type = $type == Links::TYPE_WATCH ? Links::TYPE_WATCH : Links::TYPE_DOWNLOAD;
        $link->original_url = $url;
        $link->setShortUrl();
        if($link->validate()) {
            echo $url.' - saved!';
            $link->save(0);
        } else {
            echo print_r($link->getErrors());
        }
        echo "\n";
    }

    public function exua(Films $film)
    {
        $query = urlencode($film->name . ' ' . $film->year);
        $url = "http://www.ex.ua/search?s={$query}";

        try {
            $doc = new Document($url, true);

            if ($doc->has('table.panel') && $doc->first('table.panel')->first('img') !== null) {

                if ($doc->first('table.panel')->first('img')->parent() === null) return false;
                if ($doc->first('table.panel')->first('img')->parent()->getAttribute('href') === null) return false;

                $link = $doc->first('table.panel')->first('img')->parent()->getAttribute('href');
                return "http://www.ex.ua{$link}";
            } return false;

        } catch (\ErrorException $e) {
            $this->stdout("{$e->getMessage()}\n", Console::BG_RED);
            return false;
        }


    }

    public function toloka(Films $film)
    {
        $query = urlencode($film->name . ' ' . $film->year);
        $url = "https://toloka.to/tracker.php?prev_sd=0&prev_a=0&prev_my=0&prev_n=0&prev_shc=0&prev_shf=1&prev_sha=1&prev_cg=0&prev_ct=0&prev_at=0&prev_nt=0&prev_de=0&prev_nd=0&prev_tcs=1&prev_shs=0&f%5B%5D=117&f%5B%5D=84&f%5B%5D=42&f%5B%5D=124&f%5B%5D=125&f%5B%5D=129&f%5B%5D=219&f%5B%5D=118&f%5B%5D=16&f%5B%5D=32&f%5B%5D=19&f%5B%5D=44&f%5B%5D=127&f%5B%5D=55&f%5B%5D=94&f%5B%5D=144&f%5B%5D=190&f%5B%5D=70&f%5B%5D=192&f%5B%5D=193&f%5B%5D=195&f%5B%5D=194&f%5B%5D=196&f%5B%5D=197&f%5B%5D=225&f%5B%5D=21&f%5B%5D=131&f%5B%5D=226&f%5B%5D=227&f%5B%5D=228&f%5B%5D=229&f%5B%5D=230&f%5B%5D=136&f%5B%5D=120&f%5B%5D=237&o=6&s=2&tm=-1&sns=-1&sds=-1&nm={$query}&pn=&send=%D0%9F%D0%BE%D1%88%D1%83%D0%BA";

        try {
            $doc = new Document($url, true);
            if ($doc->has('.topictitle')) {
                $link = $doc->first('.topictitle')->firstChild()->getAttribute('href');
            } else {
                return false;
            }

            return "https://toloka.to/{$link}";
        } catch (\ErrorException $e) {
            $this->stdout("{$e->getMessage()}\n", Console::BG_RED);
            return false;
        }
    }

}