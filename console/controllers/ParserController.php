<?php

namespace console\controllers;

use DiDom\Query;
use modules\actors\models\frontend\Actors;
use modules\films\models\frontend\Films;
use modules\films\models\backend\Genres;
use yii\base\ErrorException;
use yii\console\Controller;
use yii\helpers\ArrayHelper as AH;
use yii\helpers\Console;
use DiDom\Document;
use yii\helpers\Url;

//chcp 65001

class ParserController extends Controller
{
    public $count = 30;
    public $begin = 5;
    static $url = 'http://kino.net.ua/film/';

    public function actionLinks()
    {
        echo 'Hello!';
    }

    /*public function actionFiles()
    {
        $actors = Actors::find()->orderBy('id DESC')->all();
        $films = Films::find()->orderBy('id DESC')->all();
        //Переміщення файлів
        foreach ($actors as $actor) {
            $filePath = Url::to("@files/actors/{$actor->id}.jpg");
            if (file_exists($filePath)) {
                $newPath = $actor->getPosterPath();
                if (true === copy($filePath, $newPath)) {
                    unlink($filePath);
                } else {
                    $this->stdout($filePath . ": Error!\n", Console::BG_RED);
                }
            }
        }

        foreach ($films as $film) {
            $filePath = Url::to("@files/posters/{$film->id}.jpg");
            if (file_exists($filePath)) {
                $newPath = $film->getPosterPath();
                if (true === copy($filePath, $newPath)) {
                    unlink($filePath);
                } else {
                    $this->stdout($filePath . ": Error!\n", Console::BG_RED);
                }
            }
        }
    }*/

    public function actionImages()
    {
        $actors = Actors::find()->orderBy('id DESC')
            #->limit(3)
            ->all();
        $films = Films::find()->orderBy('id DESC')
            #->limit(3)
            ->all();

        /**
         * @var $actor Actors
         */
        //$i = 0;
        //$total = count($actors);
        /*
        foreach ($actors as $actor) {
            $filePath = Url::to("@files/actors/{$actor->getDirectory()}/{$actor->id}.jpg");
            if (file_exists($filePath)) {
                $this->newImage($filePath);
            } else {
                $this->stdout($filePath . ": Error!\n", Console::BG_RED);
            }

            $i++;
            $this->stdout("{$i}/{$total}\n", Console::BG_RED);
        }*/
        /**
         * @var $film Films
         */
        $i = 0;
        $total = count($films);
        foreach ($films as $film) {
            $filePath = $film->getPosterPath();
            if (file_exists($filePath)) {
                $this->newImage($filePath);
            } else {
                $this->stdout($filePath . ": not found!\n", Console::BG_YELLOW);
            }

            $i++;
            $this->stdout("{$i}/{$total}\n", Console::BG_GREY);
        }
    }

    public function newImage($path)
    {
        $ext = 'jpg';
        try {
            $image = imagecreatefromjpeg($path);
        } catch (ErrorException $e) {
            $this->stdout($path . ": It's PNG?\n", Console::BG_RED);
            try {
                $image = imagecreatefrompng($path);
                $ext = 'png';
            }catch (ErrorException $e) {
                $this->stdout("No!\n", Console::BG_RED);
                return;
            }
        }

        $image = $this->imageReflection($image);
        $image = imagerotate($image, 0.2, 0);
        $result = $ext === 'jpg' ? imagejpeg($image, $path) : imagepng($image, $path);

        if ($result === true) {
            $this->stdout($path . ": Photo uploaded\n", Console::BG_GREEN);
        } else {
            $this->stdout($path . ": Err\n", Console::BG_RED);
        }
    }

    function imageReflection($src_img, $what_direction = 'x')
    {
        $src_height = imagesy($src_img);
        $src_width = imagesx($src_img);

        $reflected = imagecreatetruecolor($src_width, $src_height);
        imagealphablending($reflected, false);
        imagesavealpha($reflected, true);

        for ($y = 1; $y <= $src_height; $y++)
        {
            for ($x = 0; $x < $src_width; $x++)
            {
                if($what_direction == 'x')
                {
                    $rgba = imagecolorat($src_img,
                        $src_width - ($x + 1),
                        $y - 1);
                }
                else if($what_direction == 'xy' || $what_direction == 'yx')
                {
                    $rgba = imagecolorat($src_img,
                        $src_width - ($x + 1),
                        $src_height - $y);
                }
                else
                {
                    $rgba = imagecolorat($src_img, $x, $src_height - $y);
                }

                $rgba = imagecolorsforindex($src_img, $rgba);

                $rgba = imagecolorallocatealpha(
                    $reflected,
                    $rgba['red'],
                    $rgba['green'],
                    $rgba['blue'],
                    $rgba['alpha']
                );

                imagesetpixel($reflected, $x, $y - 1, $rgba);
            }
        }

        return $reflected;
    }

    public function actionActors()
    {
        $actors = Actors::find()->orderBy('id DESC')->offset(1900)->all();
        $total = count($actors);
        $current = 1;
        /**
         * @var $actor Actors
         */

        foreach ($actors as $actor) {
            $this->stdout($current++ . "/$total\n", Console::BG_PURPLE);
            $filePath = $actor->getPosterPath();
            if (file_exists($filePath)) {
                $this->stdout("{$actor->name} - file already exists!\n", Console::BG_GREEN);
                continue;
            }
            if (false !== $url = $this->getActorUrl($actor)) {
                $this->loadActorFromUrl($actor, $url);
            } else {
                $this->stdout("{$actor->name} - not found!\n", Console::BG_RED);
            }

        }
    }

    public function getActorUrl(Actors $actor)
    {
        $query = urlencode($actor->name);
        $queryUrl = "http://kinoafisha.ua/ua/search/persons/?query={$query}&ref=person";
        $doc = new Document($queryUrl, true);

        if ($doc->has('.list-actors') === false && $actor->original_name !== null) {
            $query = urlencode($actor->original_name);
            $queryUrl = "http://kinoafisha.ua/ua/search/persons/?query={$query}&ref=person";
            $doc = new Document($queryUrl, true);

            if ($doc->has('.list-actors') === false) {
                //$this->stdout("{$actor->name} can't find by name and original name!\n", Console::BG_RED);
                return false;
            }
        } elseif($doc->has('.list-actors') === false) {
            //$this->stdout("{$actor->name} can't find by name!\n", Console::BG_RED);
            return false;
        }

        $actor = $doc->first('.photo')->getAttribute('href');
        $urlTemplate = '/ua/persons/';

        if (substr($actor, 0, strlen($urlTemplate)) != $urlTemplate) {
            $this->stdout("URL Template changed\n", Console::BG_RED);
            return false;
        }

        return 'http://kinoafisha.ua' . $actor;
    }

    public function loadActorFromUrl(Actors $actor, $url)
    {
        $doc = new Document($url, true);
        $bio = $doc->first('p.reviewitem');
        $img = 'http://kinoafisha.ua' . $doc->first('.photo')->getAttribute('href');
        $name = $doc->first("//span[contains(@itemprop, 'name')]", Query::TYPE_XPATH);
        $original_name = $doc->first("//span[contains(@itemprop, 'additionalName')]", Query::TYPE_XPATH);

        $local = $actor->getPosterPath();

        echo $img . "\n";

        $this->stdout("{$actor->name}\n", Console::BG_BLUE);

        if (false !== file_put_contents($local, @file_get_contents($img))) {
            $this->newImage($local); //rotate to 2 degree
            //$this->stdout("Photo uploaded\n", Console::BG_GREEN);
        }

        if ($bio !== null) {
            $actor->biography = trim($bio->text());
            //$this->stdout("Bio changed\n", Console::BG_GREEN);
        }

        if ($original_name !== null) {
            $actor->original_name = trim($original_name->text());
            //$this->stdout("Original name changed\n", Console::BG_GREEN);
        }

        if ($actor->validate()) {
            $actor->save(0);
            //$this->stdout("Actor successfully updated!\n", Console::BG_GREEN);
        } else {
            //$this->stdout(print_r($actor->getErrors()));
        }

        //$this->stdout("--------------------------\n", Console::BG_BLUE);
    }



    /**
     *
     * 1-15 Вже є
     * 500-600 вже є
     * 300-400 вже є
     * @param int $begin
     * @param int $count
     */
    public function actionIndex($begin = 706, $count = 1000)
    {
        $this->begin = $begin;
        $this->count = $count;

        $film_id = $this->begin;

        for ($i = 0; $i <= $count; $i++, $film_id++) {
            $this->stdout($this->getUrl($film_id) . "\n", Console::BOLD);
            $filmData = $this->loadFilmFromUrl($this->getUrl($film_id));

            if ($filmData !== false) {
                $this->saveFilm($filmData);
                $this->stdout("{$i} / {$count}\n", Console::BG_GREEN);
            } else {
                $this->stdout("Problems with page #{$this->getUrl($film_id)}\n", Console::BG_RED);
            }
        }

    }

    public function loadFilmFromUrl($url)
    {
        try {
            $doc = new Document($url, true);
        } catch (\ErrorException $e) {
            $this->stdout("{$e->getMessage()}\n", Console::BG_RED);
            return false;
        }

        if ($doc->has('.m-info') === false || $doc->has('.berrors') === true) {
            return false;
        }

        $title = $doc->first('title')->text();
        $pageType = mb_strtolower(mb_substr(trim($title), 0, mb_strlen('Фільм', 'UTF-8'), 'UTF-8'));

        if ($pageType !== 'фільм') {
            return false;
        }

        $data = [];
        $data['poster'] = $this->getPoster($doc);
        $data['name'] = $this->getName($doc);
        $data['original_name'] = $this->getOriginalName($doc) ? $this->getOriginalName($doc) : $data['name'];
        $data['year'] = $this->getYear($doc);
        $data['description'] = $this->getDescription($doc);

        $items = $doc->find('.info-item');

        foreach ($items as $item) {
            $text = trim($item->text());
            list($key, $value) = explode(':', $text);
            $key = mb_strtolower(trim($key), 'UTF-8');
            $value = trim($value);

            $data[$key] = $value;
        }

        return $data;
    }

    public function saveFilm($data = [])
    {
        $film = new Films();
        $film->type = $film::TYPE_FILM;

        $film->name =          AH::getValue($data, 'name');
        $film->original_name = AH::getValue($data, 'original_name');
        $film->year =          AH::getValue($data, 'year');
        $film->description =   AH::getValue($data, 'description');
        $film->country =       AH::getValue($data, 'країни');
        $film->actors =        AH::getValue($data, 'в ролях');
        $film->director =      AH::getValue($data, 'режисер');

        $film->genres = $this->getGenres(AH::getValue($data, 'жанр'));
        $film->length_time = $this->getLengthTime(AH::getValue($data, 'тривалість'));

        if ($film->validate()) {
            $film->save(false);

            $url = AH::getValue($data, 'poster');
            $local = $film->getPosterPath();
            $this->stdout("{$film->name} +", Console::BG_GREEN);

            if (false !== file_put_contents($local, @file_get_contents($this->normalizeUrl($url)))) {
                $this->stdout("Poster +\n", Console::BG_GREEN);
            }

            $this->stdout("\n");
        } else {
            $this->stdout("Validation error:", Console::BG_RED);
            $this->stdout("\n");
            $this->stdout(print_r($film->getErrors()));
            $this->stdout("\n");
        }

    }

    public function normalizeUrl($url)
    {
        $site = 'http://kino.net.ua';
        if (substr($url, 0, strlen($site)) != $site) {
            return $site . $url;
        }

        return $url;
    }

    /**
     * @param $string
     * @return array
     */
    public function getGenres($string)
    {
        $genreNames = explode('/', $string);

        if (count($genreNames) <= 1) {
            if(null !== $genre = $this->getGenreByName($string)) {
                return [$genre->id => $genre->id];
            }
        }

        $genres = [];

        foreach ($genreNames as $genreName) {
            if(null !== $genre = $this->getGenreByName($genreName)) {
                 $genres[$genre->id] = $genre->id;
            }
        }

        return $genres;
    }

    public function getLengthTime($string)
    {
        $parts = explode(';', $string);

        $parts = explode('=', $parts[0]);

        return abs(intval(trim($parts[1])));
    }

    /**
     * @param $name
     * @return Genres|null
     */
    public function getGenreByName($name)
    {
        $corrections = [
            'трилер' => 'триллер',
            'сімейний' => 'пригоди',
        ];

        if (isset($corrections[$name])) $name = $corrections[$name];

        return Genres::find()->where('name = :name', [
            'name' => trim($name),
        ])->one();
    }

    /**
     * @param $doc Document
     * @return mixed
     */
    public function getPoster($doc)
    {
        $poster = $doc->first('.highslide');
        return $poster->getAttribute('href');
    }

    /**
     * @param $doc Document
     * @return mixed
     */
    public function getName($doc)
    {
        $name = $doc->first('h1');

        $name = explode('(', $name->text());

        return trim($name[0]);
    }

    /**
     * @param $doc Document
     * @return mixed
     */
    public function getOriginalName($doc)
    {
        $content = $doc->first('#content_block');
        $name = $content->first('h4') !== null ? $content->first('h4') : '';

        if (empty($name)) return '';

        return trim($name->text());
    }

    /**
     * @param $doc Document
     * @return mixed
     */
    public function getYear($doc)
    {
        $name = $doc->first('h1');

        $name = explode('(', $name->text());

        return substr(trim($name[1]), 0, -1);
    }

    /**
     * @param $doc Document
     * @return mixed
     */
    public function getDescription($doc)
    {
        $descEl = $doc->first('.desc-text');

        $text = trim($descEl->text());

        return explode("\n", $text)[0];
    }

    public function getUrl($film_id)
    {
        return self::$url . $film_id;
    }
}