<?php

use yii\db\Schema;
use yii\db\Migration;

class m160714_085032_add_columns_to_users_table extends Migration
{
    public function up()
    {
        $this->addColumn('users', 'email', $this->string()->defaultValue(''));
        $this->addColumn('users', 'email_token', $this->string()->defaultValue(''));
        $this->addColumn('users', 'status', $this->smallInteger()->defaultValue(0));

        $this->addColumn('users', 'name', $this->string(50));
        $this->addColumn('users', 'surname', $this->string(50));
        $this->addColumn('users', 'date_of_birth', $this->string(10));
        $this->addColumn('users', 'city', $this->string(50));
        $this->addColumn('users', 'info', $this->text());
        $this->addColumn('users', 'vk', $this->string());
        $this->addColumn('users', 'fb', $this->string());
        $this->addColumn('users', 'tw', $this->string());
        $this->addColumn('users', 'ask', $this->string());
        $this->addColumn('users', 'instagram', $this->string());
        $this->addColumn('users', 'skype', $this->string());
        $this->addColumn('users', 'viber', $this->string());
        $this->addColumn('users', 'tel', $this->string());

        $this->addColumn('users', 'exp', $this->integer());
        $this->addColumn('users', 'level', $this->integer());
        $this->addColumn('users', 'coins', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%users}}', 'email');
        $this->dropColumn('{{%users}}', 'email_token');
        $this->dropColumn('{{%users}}', 'status');
        $this->dropColumn('{{%users}}', 'name');
        $this->dropColumn('{{%users}}', 'surname');
        $this->dropColumn('{{%users}}', 'date_of_birth');
        $this->dropColumn('{{%users}}', 'city');
        $this->dropColumn('{{%users}}', 'info');
        $this->dropColumn('{{%users}}', 'vk');
        $this->dropColumn('{{%users}}', 'fb');
        $this->dropColumn('{{%users}}', 'tw');
        $this->dropColumn('{{%users}}', 'ask');
        $this->dropColumn('{{%users}}', 'instagram');
        $this->dropColumn('{{%users}}', 'skype');
        $this->dropColumn('{{%users}}', 'viber');
        $this->dropColumn('{{%users}}', 'tel');
        $this->dropColumn('{{%users}}', 'exp');
        $this->dropColumn('{{%users}}', 'level');
        $this->dropColumn('{{%users}}', 'coins');
    }
}
