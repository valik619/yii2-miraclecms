<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../../vendor/autoload.php');
require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../../common/config/bootstrap.php');
require(__DIR__ . '/../config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/main.php'),
    require(__DIR__ . '/../../common/config/main-local.php'),
    require(__DIR__ . '/../config/main.php'),
    require(__DIR__ . '/../config/main-local.php')
);

$application = new yii\web\Application($config);
//$application->run();

use samdark\sitemap\Sitemap;
use samdark\sitemap\Index;

// create sitemap
$sitemap = new Sitemap(__DIR__ . '/sitemap.xml');

$films = \modules\films\models\frontend\Films::find()->all();
$actors = \modules\actors\models\frontend\Actors::find()->all();
$genres = \modules\films\models\frontend\Genres::find()->all();
$url = 'http://imperia-kino.com.ua';

$sitemap->addItem($url . '/registration', time(), Sitemap::MONTHLY, 0.5);
$sitemap->addItem($url . '/contacts', time(), Sitemap::MONTHLY, 0.3);
$sitemap->addItem($url . '/rules', time(), Sitemap::MONTHLY, 0.3);
$sitemap->addItem($url . '/dev', time(), Sitemap::MONTHLY, 0.1);

foreach ($genres as $genre) {
    $sitemap->addItem("{$url}/genres/{$genre->id}", time(), Sitemap::WEEKLY, 0.7);
}
foreach ($films as $film) {
    $sitemap->addItem("{$url}/films/{$film->id}", time(), Sitemap::WEEKLY, 1);
}
foreach ($actors as $actor) {
    $sitemap->addItem("{$url}/actors/{$actor->id}", time(), Sitemap::WEEKLY, 1);
}

// write it
$sitemap->write();

// get URLs of sitemaps written
$sitemapFileUrls = $sitemap->getSitemapUrls($url . '/');

// create sitemap for static files
$staticSitemap = new Sitemap(__DIR__ . '/sitemap_static.xml');

foreach ($films as $film) {
    $staticSitemap->addItem($url . '/files/posters/' . $film->id . '.jpg');
}
foreach ($actors as $actor) {
    $staticSitemap->addItem($url . '/files/actors/' . $actor->id . '.jpg');
}
// write it
$staticSitemap->write();

// get URLs of sitemaps written
$staticSitemapUrls = $staticSitemap->getSitemapUrls($url . '/');

// create sitemap index file
$index = new Index(__DIR__ . '/sitemap_index.xml');

// add URLs
foreach ($sitemapFileUrls as $sitemapUrl) {
    $index->addSitemap($sitemapUrl);
}

// add more URLs
foreach ($staticSitemapUrls as $sitemapUrl) {
    $index->addSitemap($sitemapUrl);
}

// write it
$index->write();