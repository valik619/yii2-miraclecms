<?php

namespace common\components;

/**
 * Class Module
 * @package common\components
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        if (\Yii::$app->id === 'app-backend') {
            $this->setViewPath('@modules/' . $this->id . '/views/backend');
            $this->setLayoutPath('@backend/views/layouts');
            $this->controllerNamespace = $this->controllerNamespace === null ?
                '\modules\\' . $this->id . '\controllers\backend' : $this->controllerNamespace;
        } else {
            $this->setViewPath('@modules/' . $this->id . '/views/frontend');
            $this->setLayoutPath('@frontend/views/layouts');
            $this->controllerNamespace = $this->controllerNamespace === null ?
                '\modules\\' . $this->id . '\controllers\frontend' : $this->controllerNamespace;
        }
    }
}
