<?php

namespace common\components;

use Yii;
use yii\helpers\Html;

/**
 * Class View
 *
 * @property $image
 * @property $keywords
 * @property $description
 *
 * @package common\components
 */
class View extends \yii\web\View
{
    public $registeredMetaTags = [];

    protected function renderHeadHtml()
    {
        $this->registerDefaultMetaTags();
        return parent::renderHeadHtml();
    }

    public function registerDefaultMetaTags()
    {
        if (isset(Yii::$app->params['defaultMetaTags']) && is_array(Yii::$app->params['defaultMetaTags'])) {
            foreach (Yii::$app->params['defaultMetaTags'] as $name => $value) {
                $this->registerMetaTagIfNotExist($name, $value);
            }
        }
    }

    protected function registerMetaTagIfNotExist($name, $value)
    {
        if (isset($this->registeredMetaTags[$name]) === false)
            $this->registerMetaTag([
                'property' => $name,
                'content' => $value,
            ]);
    }

    public function registerMetaTag($options, $key = null)
    {
        if ($key === null) {
            $this->metaTags[] = Html::tag('meta', '', $options);
        } else {
            $this->metaTags[$key] = Html::tag('meta', '', $options);
        }

        if (isset($options['name'])) {
            $this->registeredMetaTags[$options['name']] = true;
        }

        if (isset($options['property'])) {
            $this->registeredMetaTags[$options['property']] = true;
        }
    }

}