<?php
return [
    'adminEmail' => 'admin@example.com',
    'defaultMetaTags' => [
        'og:site_name' => 'MiracleCMS - powered by Yii2',
        'og:image' => '',
        'keywords' => 'php, cms, framework',
        'description' => '',
    ],
];
