<?php
/*
 * сервер: {servername}.elastictech.org
имя пользователя: {username}@{domain}
пароль: ваш пароль
порт: 25
SSL: нет
 */

return [
    'name' => 'DEV-study',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'defaultRoute' => 'main/default/index',
    'bootstrap' => [
        'modules\main\Bootstrap',
        'modules\users\Bootstrap',
        'modules\rating\Bootstrap',
        'modules\comments\Bootstrap',
    ],
    'language' => 'ru',
    'components' => [
        'view' => [
            'class' => \common\components\View::class,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [],
        ],
        'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'linkAssets' => true
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'php:d M Y',
            'datetimeFormat' => 'php:d M в H:i',
            'timeFormat' => 'php:H:i:s',
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['user'],
            'itemFile' => '@modules/rbac/data/items.php',
            'assignmentFile' => '@modules/rbac/data/assignments.php',
            'ruleFile' => '@modules/rbac/data/rules.php',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'mailer' => [
            'class' => \yii\swiftmailer\Mailer::class,
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => Swift_SmtpTransport::class,
                'host' => 'blue.elastictech.org',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username' => 'support@imperia-kino.com.ua',
                'password' => 'ExExMAD',
                'port' => '25', // Port 25 is a very common port too
                //'encryption' => 'tls', // It is often used, check your provider or mail server
            ],
        ],
    ],
];