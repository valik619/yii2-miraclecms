<?php

namespace modules\rating\controllers\frontend;

use modules\rating\models\frontend\Rating;
use Yii;
use frontend\components\Controller;

/**
 * Class DefaultController
 * @package modules\main\controllers\frontend
 */
class DefaultController extends Controller
{
    public function setJsonResponse()
    {
        Yii::$app->response->format = 'json';
        Yii::$app->request->enableCsrfValidation = false;
    }
    public function actionVote($id)
    {
        $this->setJsonResponse();

        if (Yii::$app->user->isGuest === true) {
            return [
                'success' => false,
                'message' => 'Лише зареєстрованим користувачам дозволено ставити оцінки!',
            ];
        }

        list($module, $model, $rate) = explode(':', $id);

        if (!$module || !$model || !$rate) {
            return [
                'success' => false,
                'message' => 'Некоректні дані!',
            ];
        }

        if ($rate > 10 || $rate < 1) {
            return [
                'success' => false,
                'message' => 'Оцінка може бути від 1 до 10 балів!',
            ];
        }

        $user_id = Yii::$app->user->id;
        if (Rating::find()->where(compact('module', 'model', 'user_id'))->exists() === true) {
            return [
                'success' => false,
                'message' => 'Ви вже голосували раніше!',
            ];
        }

        $vote = new Rating();
        $vote->module = $module;
        $vote->model = $model;
        $vote->rate = $rate;
        $vote->user_id = $user_id;

        if ($vote->save()) {
            return [
                'success' => true,
                'html' => \common\widgets\Rating::view($module, $model),
                'rate' => Rating::getRate($module, $model),
            ];
        } else {
            return [
                'success' => false,
                'message' => 'Помилка збереження, спробуйте пізніше або зверніться до адміністратора!',
            ];
        }
    }
}