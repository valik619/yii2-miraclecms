<?php

use yii\helpers\Html;

if (Yii::$app->user->isGuest === false) {
    \modules\rating\assets\StarsAsset::register($this);
}

/**
 * @var $module string
 * @var $id integer
 */

$divID = 'rating-' . Yii::$app->security->generateRandomString(6);

$rate = \modules\rating\models\frontend\Rating::getRate($module, $id);

?>

<?php if (Yii::$app->user->isGuest === false): ?>
    <?php $myRate = \modules\rating\models\frontend\Rating::getMyRate($module, $id); ?>
    <p>
    <div id="<?= $divID ?>">
        <div class="star-rating">
            <?php if ($myRate > 0): ?>
                <?php for ($i = 10, $val = $myRate; $i > 0; $i--): ?>
                    <?= Html::tag('span', '☆', [
                        'class' => $i <= $val ? 'blue' : null,
                    ]) ?>
                <?php endfor ?>
            <?php else: ?>
                <?php for ($i = 10; $i > 0; $i--): ?>
                    <?= Html::tag('span', '☆', [
                        'id' => "{$module}:{$id}:{$i}",
                        'data-container' => $divID,
                        'title' => 'Поставити '.$i,
                    ]) ?>
                <?php endfor ?>
            <?php endif ?>

        </div>
    </div>
    </p>
<?php endif ?>

<p id="rate-<?= $divID ?>"><strong>Рейтинг:</strong> <?= $rate ?></p>
