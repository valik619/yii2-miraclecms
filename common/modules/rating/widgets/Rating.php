<?php

namespace modules\rating\widgets;

use yii;
use yii\base\Widget;

class Rating extends Widget
{
    public static $instance = null;

    public $module;
    public $id;

    public function __construct($config = []) {
        $this->module = isset($config['module']) ? $config['module'] : null;
        $this->id = isset($config['id']) ? $config['id'] : null;
        self::$instance = $this;
        parent::__construct($config);
    }

    public static function getInstance($config = []) {
        if (self::$instance === null) {
            self::$instance = new self($config = []);
        }
        return self::$instance;
    }

    public function init()
    {
        parent::init();
    }

    public function run(){
        parent::run();
    }

    public static function view($module, $id)
    {
        $params = compact('module', 'id');
        return self::getInstance($params)->render('rating/_view', $params);
    }
}