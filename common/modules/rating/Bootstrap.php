<?php

namespace modules\rating;

use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package modules\main
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->urlManager->addRules(
            [
                'vote/<id>' => 'rating/default/vote',
            ],
            false
        );

        $app->params['dee.migration.path'][] = '@modules/rating/migrations';
    }
}
