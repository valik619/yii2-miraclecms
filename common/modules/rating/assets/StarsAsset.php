<?php

namespace modules\rating\assets;

#use themes\vem\assets\AppAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class StarsAsset extends AssetBundle
{
    public $sourcePath = '@modules/rating/assets/stars';
    public $css = ['css/stars.css'];
    public $js =  ['js/stars.js'];
    public $depends = [JqueryAsset::class];
}
