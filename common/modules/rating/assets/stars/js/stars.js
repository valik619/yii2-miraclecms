$(document).ready(function () {
    $('.star-rating > span').click(function (e) {
        var target = e.target;

        if (!target.id) {
            return;
        }

        $.ajax({
            url: "/vote/" + target.id
        })
            .done(function (response) {
                if (response.success) {
                    var container = e.target.getAttribute('data-container');

                    $('#' + container).empty().html(response.html);
                    $('#rate-' + container).remove();
                } else {
                    alert(response.message);
                }
            });
    });
});