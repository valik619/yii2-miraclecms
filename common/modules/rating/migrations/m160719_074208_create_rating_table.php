<?php

use yii\db\Schema;
use yii\db\Migration;

class m160719_074208_create_rating_table extends Migration
{
    public function up()
    {
        $this->createTable('rating', [
            'id' => $this->primaryKey(),
            'module' => $this->string()->notNull(),
            'model' => $this->string()->notNull(),
            'rate' => $this->smallInteger()->notNull(),
            'user_id' => $this->integer(),
        ]);
    }

    public function down()
    {
        return $this->dropTable('rating');
    }
}
