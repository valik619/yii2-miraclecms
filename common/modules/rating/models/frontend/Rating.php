<?php

namespace modules\rating\models\frontend;

use Yii;

/**
 * This is the model class for table "rating".
 *
 * @property integer $id
 * @property string $module
 * @property string $model
 * @property integer $rate
 * @property integer $user_id
 */
class Rating extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rating';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module', 'model', 'rate'], 'required'],
            [['rate', 'user_id'], 'integer'],
            [['module', 'model'], 'string', 'max' => 255],
        ];
    }

    public static function getMyRate($module, $model)
    {
        $user_id = Yii::$app->user->id;

        if(null !== $rate = self::find()->where(compact('module', 'model', 'user_id'))->one()) {
            return abs(intval($rate->rate));
        }

        return 0;
    }

    public static function getRate($module, $model) {
        $rates = self::find()->where(compact('module', 'model'))->all();

        if (count($rates) < 1) {
            return 0.0;
        }

        $count = 0;
        $rate = 0;
        foreach ($rates as $item) {
            $count++;
            $rate += $item->rate;
        }

        return number_format($rate/$count, 1);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'module' => 'Module',
            'model' => 'Model',
            'rate' => 'Rate',
            'user_id' => 'User ID',
        ];
    }
}
