<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Admin Panel';
$this->params['pageTitle'] = $this->title;

?>

<?= Html::a('Очистити кеш', ['flush-cache']) ?>
