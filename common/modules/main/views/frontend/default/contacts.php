<?php

use yii\helpers\Html;
use common\widgets\Films;

/* @var $this yii\web\View */
/* @var $searchModel modules\films\models\frontend\FilmsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Контакти';
$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'Контакти, адміністрація, власники, імперія, кіно, фільми, адміни, дані, інформація, зв\'язок'
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Контакти адміністрації, власників Імперії Кіно (imperia-kino.com.ua)'
]);

?>

<h1><?= Html::encode($this->title) ?></h1>

<h4 class="subheading muted">
    Питання, пропозиції, скарги.
</h4>

<p>
    Привіт, якщо у вас виникли питання, пропозиції, чи, можливо, скарги - просимо звернутись
    до адміністрації сайту по нижчевказаних контактних даних:
</p>

<dl>
    <dt>E-mail:</dt>
    <dd><a href="mailto:support@imperia-kino.com.ua">support@imperia-kino.com.ua</a></dd>
    <dd><a href="mailto:admin@imperia-kino.com.ua">admin@imperia-kino.com.ua</a></dd>
    <dd><a href="mailto:imp.kino@gmail.com">imp.kino@gmail.com</a></dd>
</dl>



<p>
    Хорошого настрою та приємного перегляду!
</p>

