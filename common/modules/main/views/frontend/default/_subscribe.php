<?php
use yii\helpers\Html;
use modules\users\models\frontend\Users;

if (isset($_GET['email'])) {
    $email = $_GET['email'];
    if (false === $valid = filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo Html::tag('div', 'Схоже що ваш емейл не коректний, перевірте поле на помилки та спробуйте ща раз', [
            'class' => 'alert error',
        ]);
    } else {
        if (Users::find()->where('email=:email', ['email' => $email])->exists() === false) {
            $user = new Users();
            $user->login = $email;
            $user->email = $email;
            $user->setPassword($email);
            $user->generateAuthKey();
            $user->generateEmailConfirmToken();
            $user->save(0);

            echo Html::tag('div', 'Дякуємо що залишили свій e-mail, ми обов\'язково сповістимо вас про запуск проекту!', [
                'class' => 'alert success',
            ]);

        } else {
            echo Html::tag('div', 'Ви вже залишали свій емейл раніше, дякуємо, проект ще не готовий, ми сповістимо вас!', [
                'class' => 'alert primary',
            ]);
        }
    }
}

if (isset($_GET['email']) && false === $valid = filter_var($email, FILTER_VALIDATE_EMAIL)) { ?>

    <div class="alert">
        <h5>
            Інформація про сайт
        </h5>
        Привіт, сайт знаходиться в розробці,
        проте вже зовсім скоро тут ви зможете знайти фільми для перегляду та завантаження без жодної реклами,
        щоб отримати сповіщення про запуск проекту залиште свій e-mail і ви матимете змогу першим користуватись
        всіма можливостями та перевагами сайту:
        <br>
        <br>
        <form method="get" class="form">
            <div class="form-item">
                <div class="controls width-50">
                    <input type="email" name="email" value="<?= Html::encode($email) ?>">
                    <button class="button primary">Сповістіть мене!</button>
                </div>
            </div>
        </form>

        <p>Підписників: <?= (Users::find()->count()+10) ?></p>

    </div>

<?php } else { ?>
    <div class="alert">
        <h5>
            Інформація про сайт
        </h5>
        Привіт, сайт знаходиться в розробці,
        проте вже зовсім скоро тут ви зможете знайти фільми для перегляду та завантаження без жодної реклами,
        щоб отримати сповіщення про запуск проекту залиште свій e-mail і ви матимете змогу першим користуватись
        всіма можливостями та перевагами сайту:
        <br>
        <br>
        <form method="get" class="form">
            <div class="form-item">
                <div class="controls width-50">
                    <input type="email" name="email" placeholder="E-mail адрес">
                    <button class="button primary">Сповістіть мене!</button>
                </div>
            </div>
        </form>
        <p>Підписників: <?= (Users::find()->count()+10) ?></p>

    </div>
<?php } ?>