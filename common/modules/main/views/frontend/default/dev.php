<?php

use yii\helpers\Html;
use common\widgets\Films;

/* @var $this yii\web\View */
/* @var $searchModel modules\films\models\frontend\FilmsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Розробники Імперії Кіно';
$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'Контакти, розробники, імперія, кіно, фільми, адміни, дані, інформація, зв\'язок, сайт'
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Сторінка розробників Імперії Кіно (imperia-kino.com.ua)'
]);

?>

<h1><?= Html::encode($this->title) ?></h1>

<h4 class="subheading muted">
    Контакти, інформація.
</h4>

<p>
    Вітаємо вас на сторінці розробників цього сайту.
    Тут ми поділимося своїми контактами, інформацією,
    даними про те хто за що відповідав та який вклад вніс в розвиток проекту,
    а також розкажемо які технології використовувались при розробці.
</p>

<dl>
    <dt>Valentin Sayik <samp>Головний розробник, програміст проекту</samp></dt>
    <dd><b>Контакти:</b></dd>
    <dd>E-mail: <a href="mailto:svbackend22@gmail.com">svbackend22@gmail.com</a></dd>
    <dd>VK: <a href="//vk.com/svbackend">vk.com/svbackend</a></dd>
    <dd>FB: <a href="//facebook.com/SaikValentyn">facebook.com/SaikValentyn</a></dd>
    <dd>TW: <a href="//twitter.com/svbackend">twitter.com/svbackend</a></dd>
</dl>

<dl>
    <dt>Svyatoslav Pintokha <samp>UX Designer / Інформаційна підтримка</samp></dt>
    <dd><b>Контакти:</b></dd>
    <dd>E-mail: <a href="mailto:support@mi-barcelona.com">support@mi-barcelona.com</a></dd>
    <dd>Site: <a href="//mi-barcelona.com">Моя Барселона</a></dd>
</dl>

<dl>
    <dt>Alexander Platonov <samp>UI/UX Designer</samp></dt>
    <dd><b>Контакти:</b></dd>
    <dd>VK: <a href="//vk.com/undent">vk.com/undent</a></dd>
    <dd>Site: <a href="http://cssui.ru">CSS UI</a></dd>
</dl>

<dl>
    <dt>Freepik & FLATICON <samp>Graphic Design</samp></dt>
    <dd><a href="http://flaticon.com">Designed by Freepik and distributed by Flaticon</a></dd>
</dl>