<?php

use yii\helpers\Html;
use common\widgets\Films;

/* @var $this yii\web\View */
/* @var $searchModel modules\films\models\frontend\FilmsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Погодження';
$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => ''
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => ''
]);

?>

<h1><?= Html::encode($this->title) ?></h1>

<h4 class="subheading muted">
    Погодження користувача.
</h4>

<p>
    Сторінка в розробці..
</p>