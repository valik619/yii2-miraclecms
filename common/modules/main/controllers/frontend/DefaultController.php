<?php

namespace modules\main\controllers\frontend;

use frontend\components\Controller;

/**
 * Class DefaultController
 * @package modules\main\controllers\frontend
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
        ];
    }

    /**
     * Главная страница
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionRules()
    {
        return $this->render('rules');
    }

    public function actionDisclaimer()
    {
        return $this->render('disclaimer');
    }

    public function actionContacts()
    {
        return $this->render('contacts');
    }

    public function actionDev()
    {
        return $this->render('dev');
    }
}