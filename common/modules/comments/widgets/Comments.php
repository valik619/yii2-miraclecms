<?php

namespace modules\comments\widgets;

use yii;
use yii\base\Widget;

class Comments extends Widget
{
    public static $instance = null;

    public $module;
    public $id;
    public $model;

    public function __construct($config = []) {
        $this->module = isset($config['module']) ? $config['module'] : null;
        $this->id = isset($config['id']) ? $config['id'] : null;
        self::$instance = $this;
        parent::__construct($config);
    }

    public static function getInstance($config = []) {
        if (self::$instance === null) {
            self::$instance = new self($config);
        }
        return self::$instance;
    }

    public function init()
    {
        parent::init();
    }

    public function run(){
        parent::run();
    }

    public static function view($module, $id)
    {
        $model = new \modules\comments\models\frontend\Comments();

        $params = compact('module', 'id', 'model');
        return self::getInstance($params)->render('comments/_master', $params);
    }

    public static function viewSingleComment(\modules\comments\models\frontend\Comments $model)
    {
        return self::getInstance()->render('comments/_comment', ['model' => $model]);
    }
}