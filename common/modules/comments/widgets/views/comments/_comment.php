<?php

use yii\helpers\Html;

/**
 * @var $model \modules\comments\models\frontend\Comments
 */

$thumb = \common\components\Thumbnail::getThumb($model->user->getAvatar(), 128, 128);

?>

<div class="comment" id="#comment<?= $model->id ?>">
    <div class="row">
        <div class="col col-2 hide-on-small">
            <img src="<?= $thumb ?>">
        </div>
        <div class="col col-10">
            <h4>
                <a href="/users/<?= $model->user->id ?>"><?= $model->user->getUsername() ?></a>
            </h4>
            <h5 class="subheading muted"><?= Yii::$app->formatter->asDatetime($model->created_at) ?></h5>
            <p>
                <?php if ($model->answer_to): ?>
                    У відповідь на <a href="#comment<?= $model->answer_to ?>">коментар</a>:
                <?php endif ?>
                <?= Html::encode($model->text) ?>
            </p>

            <?php if ($model->user_id != Yii::$app->user->id): ?>
                <p class="float-right">
                    <?= Html::a('Відповісти', '#new-comment-form', [
                        'class' => 'button small primary outline',
                        'onclick' => "prepareForAnswer('{$model->id}','{$model->user->getUsername()}')",
                    ]) ?>
                </p>
            <?php else: ?>
                    <?php if ($model->created_at > time()-60*15): #15min editable time ?>
                        <button class="button small primary">Редагувати</button>
                    <?php endif ?>
                    <?php if (Yii::$app->user->can('backend_access')): ?>
                        <button class="button small danger">Видалити</button>
                    <?php endif ?>
            <?php endif ?>

        </div>
    </div>
</div>

<?php $this->registerJs("$('#answer-{$model->id}').click(prepareForAnswer);", \yii\web\View::POS_READY) ?>