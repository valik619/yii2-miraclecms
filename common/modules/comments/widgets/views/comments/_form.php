<?php

use yii\helpers\Html;

/**
 * @var $module string
 * @var $id integer
 * @var $model \modules\comments\models\frontend\Comments
 */

$form = \yii\widgets\ActiveForm::begin([
    'fieldClass' => \common\widgets\ActiveField::class,
    'options' => [
        'class' => 'form',
        'id' => 'new-comment-form',
    ],
    'action' => \yii\helpers\Url::toRoute("/comments/new/{$module}:{$id}")
]) ?>

    <div class="row">
        <div class="col col-12">
            <div class="comment">
                <?php if (Yii::$app->getUser()->getIsGuest()): ?>
                    <p>
                        <span class="alert">
                            Лише зареєстровані користувачі можуть залишати коментарі.<br>
                            <?= Html::a('Увійдіть', '/login', ['class' => 'color-primary']) ?> під своїм акаунтом,
                            або <?= Html::a('створіть новий', '/registration') ?>.
                        </span>
                    </p>
                <?php endif ?>
                
                    <?= $form->field($model, 'text')->textarea([
                        'disabled' => Yii::$app->getUser()->getIsGuest()
                    ]) ?>
                    <p>
                        <?= $form->field($model, 'answer_to')->hiddenInput(['id' => 'answer_to'])->label(false) ?>
                        <span id="answer_username"></span>
                        <?= Html::button('Відмінити', [
                            'class' => 'button outline small hide',
                            'id' => 'answer_remove',
                        ]) ?>
                    </p>

                <p>
                    <?= Html::submitButton('Надіслати', [
                        'disabled' => Yii::$app->getUser()->getIsGuest(),
                        'href' => "/comments/new/{$module}:{$id}",
                        'class' => 'button primary',
                        'id' => 'new-comment',
                        'data-on-done' => 'new-comment',
                        'data-form-id' => 'new-comment-form',
                    ]) ?>
                </p>

            </div>
        </div>
    </div>

<?php $this->registerJs("$('#answer_remove').click(removeAnswer);", \yii\web\View::POS_READY) ?>
<?php $this->registerJs("$('#new-comment').click(handleAjaxRequest);", \yii\web\View::POS_READY) ?>

<?php $form->end() ?>