<?php

/**
 * @var $module string
 * @var $id integer
 * @var $model \modules\comments\models\frontend\Comments
 */

#\modules\comments\assets\CommentsAsset::register($this);

$comments = \modules\comments\models\frontend\Comments::find()->where([
    'module' => $module,
    'model' => $id,
])->orderBy('id DESC')->all();

?>

<?= $this->render('_form', compact('model', 'module', 'id')) ?>

<div class="comments-wrap">

    <div id="comments"></div>

    <?php foreach ($comments as $comment): ?>
        <?= $this->render('_comment', ['model' => $comment]) ?>
    <?php endforeach ?>
</div>