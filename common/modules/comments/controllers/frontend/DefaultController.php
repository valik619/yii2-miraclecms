<?php

namespace modules\comments\controllers\frontend;

use modules\comments\models\frontend\Comments;
use yii;
use frontend\components\Controller;

/**
 * Class DefaultController
 * @package modules\comments\controllers\frontend
 */
class DefaultController extends Controller
{
    public function setJsonResponse()
    {
        Yii::$app->response->format = 'json';
        Yii::$app->request->enableCsrfValidation = false;
    }
    
    public function actionCreate($id)
    {
        $this->setJsonResponse();

        if (Yii::$app->user->isGuest === true) {
            return [
                'success' => false,
                'message' => 'Лише зареєстрованим користувачам дозволено писати коментарі!',
            ];
        }

        list($module, $model) = explode(':', $id);

        if (!$module || !$model) {
            return [
                'success' => false,
                'message' => 'Некоректні дані!',
            ];
        }

        $comment = new Comments();

        if ($comment->load(Yii::$app->request->post()) === false) {
            return [
                'success' => false,
                'message' => 'Не вдалось завантажити передані дані..',
            ];
        }

        $comment->module = $module;
        $comment->model = $model;
        $comment->user_id = Yii::$app->user->id;

        if ($comment->validate()) {
            $comment->save(0);
            return [
                'html' => \modules\comments\widgets\Comments::viewSingleComment($comment),
                'success' => true,
            ];
        } else {
            return [
                'success' => false,
                'message' => $comment->getFirstError('text'),
            ];
        }

    }
}