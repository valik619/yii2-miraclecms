<?php

use yii\db\Schema;
use yii\db\Migration;

class m160726_092105_create_comments_table extends Migration
{
    public function up()
    {
        $this->createTable('comments', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'answer_to' => $this->integer(), // comments.id
            'model' => $this->integer(),
            'module' => $this->string(),
            'text' => $this->text(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('comments');
    }
}
