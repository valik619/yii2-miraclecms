<?php

namespace modules\comments;

use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package modules\comments
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->urlManager->addRules(
            [
                'comments/new/<id>' => 'comments/default/create',
            ],
            true
        );

        $app->params['dee.migration.path'][] = '@modules/comments/migrations';
    }
}
