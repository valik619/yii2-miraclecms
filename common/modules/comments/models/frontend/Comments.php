<?php

namespace modules\comments\models\frontend;

use modules\users\models\frontend\Users;
use yii;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $answer_to
 * @property integer $model
 * @property string $module
 * @property string $text
 * @property integer $updated_at
 * @property integer $created_at
 * @property Users $user
 */
class Comments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['updated_at', 'created_at', 'answer_to'], 'integer'],
            ['text', 'string', 'min' => 5],
            ['text', 'required'],
            [['module', 'model', 'user_id'], 'safe'],
            ['answer_to', 'exist', 'targetClass' => Comments::class, 'targetAttribute' => 'id']
        ];
    }

    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Користувач',
            'model' => 'Model',
            'module' => 'Module',
            'text' => 'Текст коментаря',
            'updated_at' => 'Оновлено',
            'created_at' => 'Створено',
        ];
    }

    public function behaviors()
    {
        return [
            yii\behaviors\TimestampBehavior::class,
        ];
    }
}
