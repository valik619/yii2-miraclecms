function handleAjaxRequest(e) {
    var
        $link = $(e.target),
        callUrl = $link.attr('href'),
        formId = $link.data('formId'),
        onDone = $link.data('onDone'),
        ajaxRequest, updateButton;

    e.preventDefault();

    var old_value = $link.html();

    $link.append('&nbsp;<i class="icon-spin1 animate-spin">');

    ajaxRequest = $.ajax({
        type: "post",
        dataType: 'json',
        url: callUrl,
        data: (typeof formId === "string" ? $('#' + formId).serializeArray() : null)
    });

    if (typeof onDone === "string" && ajaxCallbacks.hasOwnProperty(onDone)) {
        ajaxRequest.done(ajaxCallbacks[onDone]);
    }

}

var ajaxCallbacks = {
    'test': function (response) {
        console.log(response);
    },
    'new-comment': function (response) {
        if (response.success) {
            $("#comments-text").val('');
            $("#comments").prepend(response.html).animation('fadeIn');
        } else {
            alert(response.message);
        }

        $("#new-comment").html('Надіслати');
    },
    'new-postponed-film': function (response) {
        if (response.success) {
            $("#postpone-film").html(response.buttonText).attr('class', response.buttonClass);
        } else {
            alert(response.message);
        }
    }
};

function prepareForAnswer(answer_id, username) {
    $("#answer_to").val(answer_id);
    $("#answer_username").html("Відповідаємо: " + username);
    $("#answer_remove").show();
}

function removeAnswer() {
    $("#answer_to").val(null);
    $("#answer_username").empty();
    $("#answer_remove").hide();
}