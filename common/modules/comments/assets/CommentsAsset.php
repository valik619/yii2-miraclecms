<?php

namespace modules\comments\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class CommentsAsset extends AssetBundle
{
    public $sourcePath = '@modules/comments/assets/comments';
    public $css = ['css/comments.css'];
    public $js =  ['js/comments.js'];
    public $depends = [JqueryAsset::class];
}
