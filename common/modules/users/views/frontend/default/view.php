<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \modules\users\models\frontend\Users */

$this->title = 'Профіль ' . $model->getUsername();
$this->params['pageTitle'] = $this->title;
//$this->params['breadcrumbs'][] = ['label' => 'Користувачі', 'url' => '/users'];
$this->params['breadcrumbs'][] = $model->getUsername();

$avatar = \common\components\Thumbnail::init()->image($model->getAvatar())->crop(280, 280);

?>

<h1 class="title"><?= $model->getUsername() ?></h1>
<?php if ($model->id == Yii::$app->user->id): ?>
    <p>
        <a href="/users/<?= $model->id ?>/edit" class="button primary">
            Редагувати
        </a>
    </p>
<?php endif ?>

<div class="row">
    <div class="col col-3">
        <figure>
            <a href="<?= $model->getAvatar() ?>" target="_blank">
                <img src="<?= $model->getAvatar() ?>" alt="Аватар <?= $model->getUsername() ?>">
            </a>
            <figcaption>
                Аватар <?= $model->getUsername() ?>
            </figcaption>
        </figure>
    </div>

    <div class="col col-9">
        <p>
            <strong>Дата народження:</strong> <?= $model->date_of_birth ?><br>
            <strong>Місто:</strong> <?= $model->city ?><br>
            <strong>ВКонтакті:</strong> <?= $model->vk ?><br>
            <strong>Facebook:</strong> <?= $model->fb ?><br>
            <strong>Instagram:</strong> <?= $model->instagram ?><br>
            <strong>Twitter:</strong> <?= $model->tw ?><br>
        </p>

        <p><?= $model->info ?></p>
    </div>
</div>