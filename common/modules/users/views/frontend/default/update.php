<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \modules\users\models\frontend\Users */

$this->title = 'Редагування профілю ' . $model->getUsername();
$this->params['pageTitle'] = $this->title;
//$this->params['breadcrumbs'][] = ['label' => 'Користувачі', 'url' => '/users'];
$this->params['breadcrumbs'][] = ['label' => $model->getUsername(), 'url' => '/users/'.$model->id];
$this->params['breadcrumbs'][] = 'Редагування';

$avatar = \common\components\Thumbnail::init()->image($model->getAvatar())->crop(280, 280);

?>

<h1 class="title"><?= $model->getUsername() ?></h1>
<h2 class="subheading muted">Редагування профілю</h2>

<p>
    <a href="<?= \yii\helpers\Url::to('password') ?>" class="button danger">
        Змінити пароль
    </a>
</p>

<?php $form = \yii\widgets\ActiveForm::begin([
    'fieldClass' => \common\widgets\ActiveField::class,
    'options' => [
        'class' => 'form',
        'enctype' => 'multipart/form-data',
    ]
]) ?>

    <h3 class="subheading muted">Про вас</h3>

    <div class="row">
        <div class="col col-4">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col col-4">
            <?= $form->field($model, 'surname')->textInput() ?>
        </div>
        <div class="col col-4">
            <?= $form->field($model, 'city')->textInput() ?>
        </div>
        <div class="col col-12">
            <?= $form->field($model, 'photo')->fileInput(['id' => 'avatar-upload']) ?>
        </div>

        <div class="col col-12" id="avatar-preview">
            <img src="<?= $model->getAvatar() ?>" width="256" alt="">
        </div>

        <div class="col col-12">
            <?= $form->field($model, 'info')->textarea(['rows' => 4]) ?>
        </div>
    </div>

    <h3 class="subheading muted">Контакти</h3>

    <div class="row">
        <div class="col col-4">
            <?= $form->field($model, 'vk')->textInput() ?>
        </div>
        <div class="col col-4">
            <?= $form->field($model, 'fb')->textInput() ?>
        </div>
        <div class="col col-4">
            <?= $form->field($model, 'instagram')->textInput() ?>
        </div>
        <div class="col col-4">
            <?= $form->field($model, 'tw')->textInput() ?>
        </div>
        <div class="col col-4">
            <?= $form->field($model, 'skype')->textInput() ?>
        </div>
        <div class="col col-4">
            <?= $form->field($model, 'tel')->textInput() ?>
        </div>
    </div>

    <h3 class="subheading muted">Обліковий запис</h3>
    <div class="row">
        <div class="col col-12">
            <?= $form->field($model, 'email')->textInput([
                'type' => 'email'
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col col-6">
            <?= Html::submitButton('Зберегти', ['class' => 'button primary']) ?>
        </div>
    </div>


<?php $form->end() ?>