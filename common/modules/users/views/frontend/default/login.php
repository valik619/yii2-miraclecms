<?php

use yii\helpers\Html;
use yii\widgets\ActiveField;

/* @var $this yii\web\View */
/* @var $model \modules\users\models\frontend\LoginForm */

$this->title = 'Авторизація';
$this->params['breadcrumbs'][] = $this->title;

?>

<?php $form = \yii\widgets\ActiveForm::begin([
    'fieldClass' => \common\widgets\ActiveField::class,
    'options' => [
        'class' => 'form'
    ]
]) ?>

    <div class="row">
        <div class="col col-6">
            <h1 class="title"><?= $this->title ?></h1>
            <h4 class="subheading muted">Вхід до свого акаунту</h4>
        </div>
    </div>

    <div class="row">
        <div class="col col-6">
            <?= $form->field($model, 'login')->textInput([
                'type' => 'text',
                'autofocus' => 'autofocus',
                'placeholder' => 'E-mail або телефон',
            ])
                ->label('E-mail')
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col col-6">
            <?= $form->field($model, 'password')->passwordInput([
                'placeholder' => 'Password',
            ])
                ->label('Пароль')
            ?>
        </div>
    </div>

<?php if (isset($model->_user) && $model->_user->error_auth > $model->_auth_errors): ?>
    <div class="row col col-6">
        <?= $form->field($model, 'verifyCode')->widget(\yii\captcha\Captcha::className(), ['captchaAction' => '/users/default/captcha',
            'template' => '<p>{image}</p><div class="form item">{input}</div>',]) ?>
    </div>
<?php endif ?>


    <div class="row">
        <div class="col col-6">
            <?= Html::submitButton('Увійти', ['class' => 'button primary']) ?>
        </div>
    </div>


<?php $form->end() ?>