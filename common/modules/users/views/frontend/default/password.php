<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \modules\users\models\frontend\PasswordForm */
/* @var $user \modules\users\models\frontend\Users */

$this->title = 'Редагування паролю ' . $user->getUsername();
$this->params['pageTitle'] = $this->title;
$this->params['breadcrumbs'][] = ['label' => $user->getUsername(), 'url' => '/users/'.$user->id];
$this->params['breadcrumbs'][] = ['label' => 'Редагування', 'url' => 'edit'];
$this->params['breadcrumbs'][] = 'Пароль';

?>

<h1 class="title"><?= $user->getUsername() ?></h1>
<h2 class="subheading muted">Зміна паролю</h2>

<?php $form = \yii\widgets\ActiveForm::begin([
    'fieldClass' => \common\widgets\ActiveField::class,
    'options' => [
        'class' => 'form',
    ]
]) ?>

    <div class="row">
        <div class="col col-12">
            <?= $form->field($model, 'password')->passwordInput([
                'value' => false,
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col col-6">
            <?= Html::submitButton('Зберегти', ['class' => 'button primary']) ?>
        </div>
    </div>


<?php $form->end() ?>