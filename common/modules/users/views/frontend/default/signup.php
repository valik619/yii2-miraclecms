<?php

use yii\helpers\Html;
use yii\widgets\ActiveField;

/* @var $this yii\web\View */
/* @var $model \modules\users\models\frontend\SignupForm */

$this->title = 'Реєстрація';
$this->params['breadcrumbs'][] = $this->title;

?>

<h1 class="title"><?= $this->title ?></h1>
<h4 class="subheading muted">Створення нового акаунту</h4>

<p>
    Реєстрація на сайті відкриває нові можливості для користувачів,
    виділіть декілька хвилин щоб створити власний акаунт на сайті який дозволить вам
    обговорювати фільми чи акторів, додавати фільми в закладки для пізнішого перегляду,
    рекомендувати фільми друзям чи іншим користувачам,
    дізнаватись про новинки фільмів в улюблених жанрах і багато інших переваг,
    для цього необхідно заповнити лише мінімальну кількість інформації - E-mail та пароль:
</p>

<?php $form = \yii\widgets\ActiveForm::begin([
    'fieldClass' => \common\widgets\ActiveField::class,
    'options' => [
        'class' => 'form'
    ]
]) ?>

<div class="row">
    <div class="col col-6">
        <?= $form->field($model, 'email')->textInput([
            'type' => 'email',
            'autofocus' => 'autofocus',
            'placeholder' => 'jane.doe@example.com',
        ])
            ->label('E-mail')
            ->hint('Використовуйте лише свій email адрес, на нього буде відправлено лист з підтвердженням')
        ?>
    </div>
    <div class="col col-6">
        <?= $form->field($model, 'password')->passwordInput([
            'placeholder' => 'Password',
        ])
            ->label('Пароль')
            ->hint('Не менше 6-и символів, обирайте тяжкі для взлому паролі')
        ?>
    </div>
    <div class="col col-12">
        <p class="muted">
            <small>
                Нажимаючи кнопку "Створити акаунт" ви автоматично погоджуєтесь дотримуватися
                <a href="/rules">правил сайту</a> та приймаєте умови <a href="/disclaimer">погодження користувача</a>.
            </small>
        </p>
    </div>
</div>

<?= Html::submitButton('Створити акаунт', ['class' => 'button success']) ?>

<?php $form->end() ?>