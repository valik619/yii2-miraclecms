<?php

namespace modules\users\controllers\frontend;

use common\widgets\Alert;
use modules\users\models\frontend\ConfirmEmailForm;
use modules\users\models\frontend\LoginForm;
//use modules\users\models\frontend\PasswordResetRequestForm;
//use modules\users\models\frontend\PasswordResetForm;
use modules\users\models\frontend\PasswordForm;
use modules\users\models\frontend\SignupForm;
use modules\users\models\frontend\Users;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'login'],
                'rules' => [
                    [
                        'actions' => ['signup', 'login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionView($id)
    {
        if (null === $model = Users::findOne($id)) {
            throw new NotFoundHttpException('Такого користувача не існує!');
        }

        return $this->render('view', ['model' => $model]);
    }

    /**
     * @var $model Users
     * @param $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        if (null === $model = Users::findOne($id)) {
            throw new NotFoundHttpException('Такого користувача не існує!');
        }

        if (Yii::$app->user->can('users_crud') === false && $model->id !== Yii::$app->user->id) {
            throw new ForbiddenHttpException('Недостатньо прав!');
        }

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $model->photo = UploadedFile::getInstance($model, 'photo');

            if ($model->validate()) {
                $model->uploadAvatar();
                $model->save(0);
                Alert::add('Зміни успішно збережені!', 'success');
                return $this->redirect(Url::current());
            }

        }

        return $this->render('update', ['model' => $model]);
    }

    public function actionPassword($id)
    {
        if (null === $model = Users::findOne($id)) {
            throw new NotFoundHttpException('Такого користувача не існує!');
        }

        if (Yii::$app->user->can('users_crud') === false && $model->id !== Yii::$app->user->id) {
            throw new ForbiddenHttpException('Недостатньо прав!');
        }

        $form = new PasswordForm();

        if (Yii::$app->request->isPost && $form->load(Yii::$app->request->post())) {
            if ($form->validate()) {
                Alert::add('Ваш пароль успішно змінено!', 'success');
                $model->setPassword($form->password);
                $model->save(0);
                return $this->redirect(Url::current());
            }
        }

        return $this->render('password', ['user' => $model, 'model' => $form]);
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                Yii::$app->getUser()->login($user, 86400*30);
                return $this->goHome();
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionEmailConfirm($token)
    {
        try {
            $model = new ConfirmEmailForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->confirmEmail()) {
            Alert::add('Вітаємо! Ваш E-mail успішно підтверджено!', 'success', 'Підтвердження E-mail');
        } else {
            Alert::add('Не вдалось підтвердити ваш e-mail, можливо ви зробили це раніше', 'danger');
        }

        return $this->goHome();
    }

    public function actionPasswordResetRequest()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Спасибо! На ваш Email было отправлено письмо со ссылкой на восстановление пароля.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Извините. У нас возникли проблемы с отправкой.');
            }
        }

        return $this->render('passwordResetRequest', [
            'model' => $model,
        ]);
    }

    public function actionPasswordReset($token)
    {
        try {
            $model = new PasswordResetForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'Спасибо! Пароль успешно изменён.');

            return $this->goHome();
        }

        return $this->render('passwordReset', [
            'model' => $model,
        ]);
    }
}