<?php

namespace modules\users;

use yii\base\BootstrapInterface;
use Yii;

/**
 * Class Bootstrap
 * @package modules\users
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $rules = [];
        $rules['login'] = 'users/default/login';
        $rules['logout'] = 'users/default/logout';
        $rules['registration'] = 'users/default/signup';
        $rules['users/<id:\d+>'] = 'users/default/view';
        $rules['users/<id:\d+>/edit'] = 'users/default/update';
        $rules['users/<id:\d+>/password'] = 'users/default/password';
        $rules['users/email-confirm'] = 'users/default/email-confirm';

        if (Yii::$app->id == 'app-backend') {
            $rules['users/create'] = 'users/default/create';
            $rules['users/update/<id:\d+>'] = 'users/default/update';
            $rules['users/delete/<id:\d+>'] = 'users/default/delete';
        } else {
            //$rules['users/update'] = 'users/user/update';
        }

        $app->urlManager->addRules($rules, true);
    }
}
