<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user \modules\users\models\frontend\Users */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['users/email-confirm', 'token' => $user->email_token]);
$siteUrl = Yii::$app->urlManager->createAbsoluteUrl(['/']);
?>
<p>
    Привіт, вітаємо в дружній компанії кіноманів, ми раді що ви долучаєтесь до нас,
    сподіваємось вам сподобається проводити час за переглядом та обговренням кращих
    фільмів українською мовою.
</p>

<p>
    Для відкриття всіх можливостей сайту вам потрібно підтвердити акаунт,
    для цього необхідно перейти за наступним посиланням:
    <br>
    <?= Html::a(Html::encode($confirmLink), $confirmLink) ?>
</p>

<p>
    Дані для входу на imperia-kino.com.ua<br>
    E-mail: <?= $user->email ?><br>
    Пароль: <?= $password ?><br>
</p>

<small>Якщо це були не ви, то просимо проігнорувати цей лист.</small>

<br>
<br>

З повагою,<br>
<?= Html::a('Імперія Кіно', $siteUrl) ?>