<?php

namespace modules\users\models\frontend;

use yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\Url;


class PasswordForm extends yii\base\Model
{
    public $password;

    public function rules()
    {
        return [
            ['password', 'string', 'min' => 6],
            ['password', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Новий пароль',
        ];
    }
}