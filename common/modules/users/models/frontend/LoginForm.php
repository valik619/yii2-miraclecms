<?php

namespace modules\users\models\frontend;

use modules\users\models\frontend\Users;
use yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $login;
    public $password;
    public $rememberMe = true;
    public $verifyCode;

    private $_user = null;
    private $_auth_errors = 4;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
            ['verifyCode', 'required', 'when' => function ($model) {
                return isset($model->_user->error_auth) && $model->_user->error_auth > $this->_auth_errors;
            }
            ],
            ['verifyCode', 'captchaValidate', 'skipOnEmpty' => false, 'when' => function ($model) {
                return isset($model->_user->error_auth) && $model->_user->error_auth > $this->_auth_errors;
            }],
        ];
    }

    public function captchaValidate($attribute, $params)
    {
        $validator = yii\validators\Validator::createValidator('captcha', $this, $attribute, [
            'captchaAction' => 'users/default/captcha'
        ]);

        if ($validator->validate($this->verifyCode) === false) {
            $this->addError($attribute, 'Невірний код з картинки');
        } else {
            $this->_user->error_auth = 0;
            $this->_user->save(0);
        }
    }

    /**
     * Validates the username and password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user) {
                $this->addError('login', 'Схоже що такого користувача не існує..');
            } elseif (!$user->validatePassword($this->password)) {

                ++$user->error_auth;
                $user->save(0);

                $this->addError('password', 'Пароль не вірний. У вас ' . ($res = ($this->_auth_errors-$user->error_auth)) && $res > 1 ? "залишилось {$res} спроби" : "залишилась остання спроба" . '.');
            } elseif ($user && $user->status == Users::STATUS_BLOCKED) {
                $this->addError('login', 'Ваш акаунт заблоковано.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $this->getUser()->error_auth = 0;
            $this->getUser()->save(0);
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 3600 * 24 * 7);
        } else {
            return false;
        }
    }

    /**
     * @return array|bool|Users
     */
    public function getUser()
    {
        if (!$this->_user) {
            $this->_user = Users::findByLogin($this->login);
        }

        return $this->_user;
    }

    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Код з картинки',
        ];
    }
}