<?php

namespace modules\users\models\frontend;

use common\widgets\Alert;
use yii\base\Model;
use yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $rules;

    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

        ];
    }

    public function find()
    {
        return Users::find();
    }

    /**
     * Signs user up.
     *
     * @return Users|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            
            $user = new Users();
            $user->email = $this->email;
            $user->login = $this->email;
            $user->status = Users::STATUS_WAIT;
            $user->sex = 'm';
            $user->role = 'user';
            $user->generateAuthKey();
            $user->generateEmailConfirmToken();
            $user->setPassword($this->password);
            
            if ($user->save()) {
                $user->setRandomAvatar();
                Yii::$app->mailer
                    ->compose('@modules/users/mails/confirm', [
                        'user' => $user,
                        'password' => $this->password
                        ])
                    ->setFrom('support@imperia-kino.com.ua')
                    ->setTo($this->email)
                    ->setSubject('Підтвердження реєстрації на Імперії Кіно')
                    ->send();

                Alert::add('Тепер для вас відкриті всі можливості сайту, 
                     сподіваємось вам тут сподобається і ми побачимось ще не один раз :)',
                    'success',
                    'Ви успішно зареєструвались!');
                Alert::add('Добавте трішки більше інформації про себе,
                змініть свій аватар,
                та не забудьте підтвердити вашу E-mail адресу.', 'primary', 'Нагадування');
            }

            return $user;
        }

        return null;
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
        ];
    }
}