<?php

namespace modules\users\models\frontend;

use yii\base\InvalidParamException;
use yii\base\Model;
use yii;

class ConfirmEmailForm extends Model
{
    /**
     * @var $_user Users
     */
    private $_user;

    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Код підтвердження не передано.');
        }

        $this->_user = Users::findByEmailConfirmToken($token);

        if (!$this->_user) {
            throw new InvalidParamException('Користувача з таким кодом підтвердження не знайдено, 
            можливо ви вже активували свій акаунт раніше.');
        }

        parent::__construct($config);
    }


    public function confirmEmail()
    {
        $user = $this->_user;
        $user->status = Users::STATUS_ACTIVE;
        $user->removeEmailConfirmToken();

        return $user->save();
    }
}