<?php

namespace modules\users\models\frontend;

use yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\Url;

/**
 * Class Users
 * @package modules\users\models\frontend
 *
 * @property string $id
 * @property string $login
 * @property string $password
 * @property string $auth_key
 * @property string $time_reg
 * @property string $time_login
 * @property string $ip
 * @property string $ua
 * @property string $role
 * @property string $sex
 * @property string $error_auth
 * @property string $avatar
 *
 * Class Users
 * @property $email
 * @property $date_of_birth
 * @property $city
 * @property $info
 * @property $vk
 * @property $fb
 * @property $tw
 * @property $ask
 * @property $instagram
 * @property $skype
 * @property $viber
 * @property $tel
 * @property $exp
 * @property $level
 * @property $coins
 */
class Users extends ActiveRecord implements IdentityInterface
{
    static $avatar_path = '@files/users/avatars';
    static $avatar_url = '@urlFiles/users/avatars';

    public $photo;

    public static function tableName()
    {
        return '{{%users}}';
    }

    const STATUS_BLOCKED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_WAIT = 2;


    public function getStatusName()
    {
        return yii\helpers\ArrayHelper::getValue(self::getStatusesArray(), $this->status);
    }

    public static function getStatusesArray()
    {
        return [
            self::STATUS_BLOCKED => 'Акаунт Заблокований',
            self::STATUS_ACTIVE => 'Активний',
            self::STATUS_WAIT => 'Очікує підтвердження',
        ];
    }

    public function getUsername()
    {
        if ($this->name && $this->surname) {
            return sprintf('%s %s', $this->name, $this->surname);
        }

        if ($this->email) {
            return $this->email;
        }

        return $this->login;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique'],
            ['email', 'string', 'max' => 255],
            [['name','surname','city','vk','fb','tw','viber','skype','instagram'], 'string', 'max' => 255],
            ['info', 'string'],
            ['password', 'string', 'min' => 6],
            ['tel', 'string', 'max' => 13],
            [['photo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function getAvatar()
    {
        $file = $this->getAvatarPath();

        if (file_exists($file) === false) {
            $this->setRandomAvatar();
        }

        return Url::to(self::$avatar_url . "/{$this->getDirectory()}/{$this->id}.png");
    }

    public function getAvatarPath()
    {
        $dirPath = Url::to(self::$avatar_path . "/{$this->getDirectory()}");

        if (file_exists($dirPath) === false) {
            yii\helpers\FileHelper::createDirectory($dirPath, 755, true);
            $this->setRandomAvatar();
        }

        return Url::to(self::$avatar_path . "/{$this->getDirectory()}/{$this->id}.png");
    }

    public function getDirectory()
    {
        $dirString = md5($this->id);
        $part1 = substr($dirString, 0, 2);
        $part2 = substr($dirString, 2, 2);

        return $part1 . '/' . $part2;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @param $login
     * @return array|Users
     */
    public static function findByLogin($login)
    {
        return static::find()->where([
            'email' => $login,
        ])->orWhere([
            'tel' => $login,
        ])->orWhere([
            'login' => $login,
        ])->one();
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function setRandomAvatar()
    {
        if (empty($this->sex)) $this->sex = 'm';

        $file = mt_rand(1, 10) . '.png';
        $path = yii\helpers\Url::to("@files/avatars/{$this->sex}/{$file}");
        copy($path, $this->getAvatarPath());

        return $this;
    }
    
    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'email_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = 86400;
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->email_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->email_token = null;
    }

    /**
     * @param string $email_confirm_token
     * @return static|null
     */
    public static function findByEmailConfirmToken($email_confirm_token)
    {
        return static::findOne(['email_token' => $email_confirm_token, 'status' => self::STATUS_WAIT]);
    }

    /**
     * Generates email confirmation token
     */
    public function generateEmailConfirmToken()
    {
        $this->email_token = Yii::$app->security->generateRandomString();
    }

    /**
     * Removes email confirmation token
     */
    public function removeEmailConfirmToken()
    {
        $this->email_token = null;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->generateAuthKey();
            }
            return true;
        }
        return false;
    }

    public function uploadAvatar()
    {
        if ($this->validate() && $this->photo !== null) {
            return $this->photo->saveAs($this->getAvatarPath());
        } else return false;
    }

    /**
     * Видалення файлу постеру
     * @return bool
     */
    public function deleteAvatar()
    {
        $file = $this->getAvatarPath();
        if (file_exists($file)) {
            return unlink($file);
        } else return true;
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Ім\'я',
            'surname' => 'Фамілія',
            'email' => 'E-mail',
            'photo' => 'Фото / аватар',
            'info' => 'Декілька слів про себе',
            'vk' => 'Вконтакті',
            'fb' => 'Facebook',
            'tw' => 'Twitter',
            'skype' => 'Skype',
            'viber' => 'Viber',
            'city' => 'Місто',
            'tel' => 'Номер телефону',
        ];
    }
}