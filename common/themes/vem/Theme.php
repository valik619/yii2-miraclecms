<?php

namespace themes\vem;

use Yii;

class Theme extends \yii\base\Theme
{
    /**
     * @inheritdoc
     */
    public $pathMap = [
        '@frontend/views' => '@themes/vem/views',
        '@modules' => '@themes/vem/modules'
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
