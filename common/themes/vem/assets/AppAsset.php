<?php

namespace themes\vem\assets;

use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class AppAsset extends AssetBundle
{
    public $sourcePath = '@themes/vem/assets';
    public $css = [
        'css/starter.css',
    ];
    public $js = [];

    public $depends = [
        BootstrapAsset::class,
        JqueryAsset::class,
    ];
}
