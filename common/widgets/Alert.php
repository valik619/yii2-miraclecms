<?php

namespace common\widgets;

use yii\base\Widget;

class Alert extends Widget
{
    public static $instance = null;

    public function __construct($config = []) {
        self::$instance = $this;
        parent::__construct($config);
    }

    public static function getInstance($config = []) {
        if (self::$instance === null) {
            self::$instance = new self($config = []);
        }
        return self::$instance;
    }

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $alerts = \Yii::$app->session->getFlash('alert', []);

        foreach ($alerts as $alert) {
            echo $alert;
        }
    }

    public static function add($text, $color = 'primary', $title = false)
    {
        $session = \Yii::$app->session;

        $alert = self::getInstance()->render('@widgets/views/alert/view', [
            'title' => $title,
            'text' => $text,
            'class' => $color,
        ]);

        $alerts = $session->getFlash('alert', []);
        array_push($alerts, $alert);

        $session->setFlash('alert', $alerts);
    }
}