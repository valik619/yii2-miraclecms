<?php
/**
 * @var $title string
 * @var $class string
 * @var $text string
 */
?>

<div class="alert <?= $class ?>" data-component="alert">
    <span class="close small"></span>

    <?php if ($title !== false): ?>
        <h5><?= $title ?></h5>
    <?php endif ?>
    <?= $text ?>
</div>