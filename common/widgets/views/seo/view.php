<?php
/**
 * @var $title string
 * @var $subtitle string
 * @var $text string
 */
?>

<div class="alert seo">
    <h1>
        <?= $title ?>
    </h1>
    <h2 class="subheading muted">
        <?= $subtitle ?>
    </h2>
    <?= $text ?>
</div>
