<?php

namespace common\widgets;

use yii\base\Widget;

class Seo extends Widget
{
    public static $instance = null;

    public function __construct($config = []) {
        self::$instance = $this;
        parent::__construct($config);
    }

    public static function getInstance($config = []) {
        if (self::$instance === null) {
            self::$instance = new self($config = []);
        }
        return self::$instance;
    }

    public function init()
    {
        parent::init();
    }

    public function run(){
        parent::run();
    }

    public static function view($title, $subtitle, $text)
    {
        return self::getInstance()->render('@widgets/views/seo/view', [
            'title' => $title,
            'subtitle' => $subtitle,
            'text' => $text,
        ]);
    }
}